--This is a post HOOK
local data = SkillTreeTweakData.init
function SkillTreeTweakData:init(tweak_data)
	data(self, tweak_data)
    
    for tree_idx in pairs(self.trees) do
		local tree = self.trees[tree_idx]
        if tree.name_id == "st_menu_technician_sentry" then 
            tree.tiers = {
				{"defense_up"},
				{
					"sentry_targeting_package",
					"eco_sentry"
				},
				{
					"engineering",
					"jack_of_all_trades"
				},
				{
					"tower_defense", "less_is_better"
				}
			}
        end
	end

    self.skills.defense_up = {
		["name_id"] = "menu_predator_defense_up",
		["desc_id"] = "menu_predator_defense_up_desc",
		["icon_xy"] = {9, 0},
		[1] = {
			upgrades = {
				"sentry_gun_cost_reduction_1", "sentry_gun_mark_boost_1"
			},
			cost = self.costs.hightier
		},
		[2] = {
			upgrades = {
				"sentry_gun_shield" , "sentry_gun_mark_boost_2"
			},
			cost = self.costs.hightierpro
		}
    }
    
    self.skills.engineering = {
		["name_id"] = "menu_engineering_improved",
		["desc_id"] = "menu_engineering_improved_desc",
		["icon_xy"] = {9, 3},
		[1] = {
			upgrades = {
				"sentry_gun_silent", "auto_pickup_broken_sentrys"
			},
			cost = self.costs.hightier
		},
		[2] = {
			upgrades = {
				"sentry_gun_ap_bullets",
				"sentry_gun_fire_rate_reduction_1"
			},
			cost = self.costs.hightierpro
		}
	}

    self.skills.eco_sentry = {
		["name_id"] = "menu_angry_sentry",
		["desc_id"] = "menu_angry_sentry_desc",
		["icon_xy"] = {9, 2},
		[1] = {
			upgrades = {
				"sentry_gun_cost_reduction_2", "explosion_on_death_1"
			},
			cost = self.costs.hightier
		},
		[2] = {
			upgrades = {
				"sentry_gun_armor_multiplier", "explosion_on_death_2"
			},
			cost = self.costs.hightierpro
		}
    }
    
    self.skills.less_is_better = {
        name_id = "menu_less_is_better",
		desc_id = "menu_less_is_better_desc",
		icon_xy = {
			7,
			6
		},
		{
			upgrades = {"sentry_gun_quantity_1"},
			cost = self.costs.hightier
		},
		{
			upgrades = {"sentry_gun_quantity_2", "sentry_gun_quantity_cap", "sentry_gun_ap_rof_buff"}, -- sentry_gun_cost_reduction_3
			cost = self.costs.hightierpro
		}
	}

end
