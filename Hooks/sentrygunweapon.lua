--This is a HOOK
local data_fire_raycast = SentryGunWeapon._fire_raycast
function SentryGunWeapon:_fire_raycast(from_pos, direction, shoot_player, target_unit)
    -- mark specials
    if managers.player:has_category_upgrade("sentry_gun", "mark_boost") and managers.groupai:state():is_enemy_special(target_unit) then
        if managers.player:upgrade_value("sentry_gun", "mark_boost", 1) == 1 then
            target_unit:contour():add("mark_enemy", Menumx_sentry:get_data(), 1)
        else
            target_unit:contour():add("mark_enemy_damage_bonus", Menumx_sentry:get_data(), 1)
        end
    end
    return data_fire_raycast(self,from_pos, direction, shoot_player, target_unit)
end

Hooks:PostHook(SentryGunWeapon, "_set_fire_mode", "_set_fire_mode_mod", function(self, use_armor_piercing)
    if use_armor_piercing and managers.player:has_category_upgrade("sentry_gun", "ap_rof_buff") then
        self._fire_rate_reduction = self._use_armor_piercing and self._AP_ROUNDS_FIRE_RATE / 1.70 or 1
        -- chat_log(self._fire_rate_reduction)
    end
end)
