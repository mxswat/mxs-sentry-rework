_G.Menumx_sentry = _G.Menumx_sentry or {}
Menumx_sentry._path = ModPath
Menumx_sentry._data_path = ModPath .. "/save/Menumx_sentry_options.txt"
Menumx_sentry._data = {}
--[[
    okay, so, add your skills ids from the skilltreetweakdata in the banned_skill_couple var
    
    you have to add the two skill ids like in the example below
]]
Menumx_sentry.banned_skill_couple = {
	["tower_defense"] = "less_is_better"
}
-- If you want you can localize the text but you can leave it empty or like it is now
Menumx_sentry.banned_skill_loc = {
    ["tower_defense"] = "TOWER DEFENSE",
    ["less_is_better"] = "LESS IS BETTER"
}

-- I revert the table to make the search faster
local s={}
for k,v in pairs(Menumx_sentry.banned_skill_couple) do
    s[v]=k
end
Menumx_sentry.banned_skill_couple_revert = s

function Menumx_sentry:get_data()
	return Menumx_sentry._data.mx_sentry_val
end

function Menumx_sentry:Save()
	local file = io.open( self._data_path, "w+" )
	if file then
		file:write( json.encode( self._data ) )
		file:close()
	end
end
function Menumx_sentry:Load()
	local file = io.open( self._data_path, "r" )
	if file then
		self._data = json.decode( file:read("*all") )
		file:close()
		if self._data.mx_sentry_val == nil then self._data.mx_sentry_val = true end
		Menumx_sentry:Save()
	end
end
Hooks:Add("MenuManagerInitialize", "Menumx_sentry_MenuManagerInitialize", function(menu_manager)
	MenuCallbackHandler.Menumx_sentry_save = function(self, item)
		Menumx_sentry:Save()
	end	
	MenuCallbackHandler.mx_sentry_func = function(self, item)
		Menumx_sentry._data.mx_sentry_val = (item:value() == "on" and true or false)
	end		
	Menumx_sentry:Load()
	MenuHelper:LoadFromJsonFile(Menumx_sentry._path .. "Hooks/options.txt", Menumx_sentry, Menumx_sentry._data)
end )