local data = UpgradesTweakData.init
function UpgradesTweakData:init(tweak_data)
	data(self, tweak_data)

    -- explosion_on_death
        self.values.sentry_gun.explosion_on_death = {45,75}
        
        self.definitions.explosion_on_death_1 = {
            category = "feature",
            name_id = "menu_sentry_gun_explosion_on_death_1",
            upgrade = {
                category = "sentry_gun",
                upgrade = "explosion_on_death",
                value = 1
            }
        }
        
        self.definitions.explosion_on_death_2 = {
            category = "feature",
            name_id = "menu_sentry_gun_explosion_on_death_1",
            upgrade = {
                category = "sentry_gun",
                upgrade = "explosion_on_death",
                value = 2
            }
        }
    -- explosion_on_death end

    -- auto_pickup_broken_sentrys
        self.values.sentry_gun.auto_pickup_broken_sentrys = {true}

        self.definitions.auto_pickup_broken_sentrys = {
            category = "feature",
            name_id = "menu_sentry_gun_auto_pickup_broken_sentrys",
            upgrade = {
                category = "sentry_gun",
                upgrade = "auto_pickup_broken_sentrys",
                value = 4
            }
        }
    -- auto_pickup_broken_sentrys end
    
    -- sentry_gun_mark_boost
        self.values.sentry_gun.mark_boost = {1,2}
        self.definitions.sentry_gun_mark_boost_1 = {
                category = "feature",
                incremental = true,
                name_id = "menu_sentry_gun_mark_boost",
                upgrade = {
                    category = "sentry_gun",
                    upgrade = "mark_boost",
                    value = 1
                }
            }

        self.definitions.sentry_gun_mark_boost_2 = {
            category = "feature",
            incremental = true,
            name_id = "menu_sentry_gun_mark_boost",
            upgrade = {
                category = "sentry_gun",
                upgrade = "mark_boost",
                value = 2
            }
        }
    -- sentry_gun_mark_boost end
    
    self.values.sentry_gun.quantity_cap = {true}

    -- sentry_gun_quantity_cap
    self.definitions.sentry_gun_quantity_cap = {
        category = "feature",
        incremental = true,
        name_id = "menu_sentry_gun_sentry_gun_quantity_cap",
        upgrade = {
            category = "sentry_gun",
            upgrade = "quantity_cap",
            value = 1
        }
    }

    -- even less cost
    self.values.sentry_gun.cost_reduction = {2, 3,0,4}
	self.definitions.sentry_gun_cost_reduction_3 = {
		name_id = "menu_sentry_gun_cost_reduction",
		category = "feature",
		upgrade = {
			value = 4,
			upgrade = "cost_reduction",
			category = "sentry_gun"
		}
	}
    -- rof bonus 
   
    self.values.sentry_gun.ap_rof_buff = {true}
	self.definitions.sentry_gun_ap_rof_buff = {
		incremental = true,
		name_id = "menu_sentry_gun_ap_rof_buff",
		category = "feature",
		upgrade = {
			value = 1,
			upgrade = "ap_rof_buff",
			category = "sentry_gun"
		}
    }
    -- sentry_gun_damage_boost
        self.values.sentry_gun.damage_boost = {1.15,1.30} 
        self.definitions.sentry_gun_damage_boost_1 = {
                category = "feature",
                incremental = true,
                name_id = "menu_sentry_gun_damage_boost",
                upgrade = {
                    category = "sentry_gun",
                    upgrade = "damage_boost",
                    value = 1
                }
            }

        self.definitions.sentry_gun_damage_boost_2 = {
            category = "feature",
            incremental = true,
            name_id = "menu_sentry_gun_damage_boost",
            upgrade = {
                category = "sentry_gun",
                upgrade = "damage_boost",
                value = 2
            }
        }
    -- sentry_gun_damage_boost
end


