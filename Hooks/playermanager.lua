local data_add_equipment_amount = PlayerManager.add_equipment_amount
function PlayerManager:add_equipment_amount(equipment, amount, slot)
    local amount_to_send = amount
    local data, index = self:equipment_data_by_name(equipment)
    if data and managers.player:has_category_upgrade("sentry_gun", "quantity_cap") and (equipment == "sentry_gun" or  equipment == "sentry_gun_silent") then
        amount_to_send = math.max(0, math.min(2, amount)) -- clamp in lua *shruggie*
    end
    data_add_equipment_amount(self, equipment, amount_to_send, slot)
end
