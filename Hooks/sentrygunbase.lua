
SentryGunBase = SentryGunBase or class(UnitBase)
SentryGunBase.DEPLOYEMENT_COST = {
	0.7,
	0.75,
	0.8,
	1.0
}

SentryGunBase.MIN_DEPLOYEMENT_COST = 0.0


function SentryGunBase:on_death()
    if managers.player:has_category_upgrade("sentry_gun", "auto_pickup_broken_sentrys") then
        if self._unit:interaction() then
            self._unit:interaction():interact()
        end
    end
    
    if managers.player:has_category_upgrade("sentry_gun", "explosion_on_death") then
        local bodies = World:find_units_quick("sphere", self._unit:position(), '750', managers.slot:get_mask("enemies"))
        local col_ray = { }
        col_ray.ray = Vector3(1, 0, 0)
        col_ray.position = self._unit:position()
        local action_data = {
            variant = "explosion",
            damage = managers.player:upgrade_value("sentry_gun", "explosion_on_death", 1),
            attacker_unit = managers.player:player_unit(),
            col_ray = col_ray
        }
        for _, hit_unit in ipairs(bodies) do
            if hit_unit:character_damage() then
                hit_unit:character_damage():damage_explosion(action_data)
            end
        end
    end

	self._unit:set_extension_update_enabled(Idstring("base"), false)
	self:unregister()
end

--[[
Hooks:PostHook(SentryGunBase, "setup", "setup_mod", function(self, owner, ammo_multiplier, ...)
    local ammo_amount = tweak_data.upgrades.sentry_gun_base_ammo * ammo_multiplier
    chat_log("ammo_amount: "..tostring(ammo_amount))
end)
]]
